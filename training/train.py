# from fastai.vision import (
#     cnn_learner,
#     accuracy,
#     ImageDataBunch,
#     models,
#     get_transforms,
#     ImageList,
#     cifar_stats,
#     LabelSmoothingCrossEntropy,
#     defaults, Path
# )
import torchvision.models as tmodels
from fastai.vision import *
from fastai.basic_train import Learner
from fastai.torch_core import optim
from fastai.script import Param, call_parse
from fastai.callbacks import CSVLogger
from fastai.metrics import top_k_accuracy
from functools import partial
import numpy as np
from fastprogress import fastprogress

torch.backends.cudnn.benchmark = True
fastprogress.MAX_COLS = 80
#from archs.mxresnet import *
#from archs.ranger import Ranger

def get_data(path, partial_data, size, bs):
    print('gathering data')
    tfms = get_transforms(max_rotate=.0, p_affine=0.75)

    data = (ImageList.from_folder(path)
            .use_partial_data(partial_data)
            .split_by_rand_pct(valid_pct=0.2)            
            .label_from_folder()
            .transform(tfms=tfms, size=size) #future add tfms
            .databunch(bs=bs, no_check=True))
    img_stats = data.batch_stats()
    data.normalize(img_stats)
    return data

def fit_with_annealing(learn:Learner, num_epoch:int, lr:float=defaults.lr, annealing_start:float=0.7)->None:
    n = len(learn.data.train_dl)
    anneal_start = int(n*num_epoch*annealing_start)
    phase0 = TrainingPhase(anneal_start).schedule_hp('lr', lr)
    phase1 = TrainingPhase(n*num_epoch - anneal_start).schedule_hp('lr', lr, anneal=annealing_cos)
    phases = [phase0, phase1]
    sched = GeneralScheduler(learn, phases)
    learn.callbacks.append(sched)
    learn.fit(num_epoch)

def train(
        lr: Param("Learning rate", float)=1e-3,
        opt: Param("Optimizer (adam,ranger)", str)='adam',
        alpha: Param("Alpha", float)=0.99,
        mom: Param("Momentum", float)=0.9,
        eps: Param("epsilon", float)=1e-6,
        epochs: Param("Number of epochs", int)=5,
        bs: Param("Batch size", int)=64,
        arch: Param("Architecture (xresnet34, xresnet50)", str)='xresnet50',
        sa: Param("Self-attention", int)=0,
        sym: Param("Symmetry for self-attention", int)=0,
        dump: Param("Print model; don't train", int)=0,
        lrfinder: Param("Run learning rate finder; don't train", int)=0,
        sched_type: Param("LR schedule type", str)='one_cycle',
        img_size: Param("Size of image", int)=224,
        path: Param("Path to data", str)='data/cameras_png',
        partial_data: Param("Portion of data used for training", float)=0.2,
        log: Param("Log file name", str)='log',
        ann_start: Param("Mixup", float)=-1.0,
        save: Param("Save trained model", int)=0,
        name: Param("Name to save", str)='stage-1',  
        freeze: Param("freeze pretrained model", int)=0,
    ):

    if   opt=='adam' : opt_func = partial(optim.Adam, betas=(mom,alpha), eps=eps)
    elif opt=='ranger'  : opt_func = partial(Ranger,  betas=(mom,alpha), eps=eps)

    path = Path(path)
    data = get_data(path, partial_data, img_size, bs)
    
    print('Downloading model')
    if arch=='dense': 
        model = tmodels.densenet161(pretrained=True)
    elif arch=='resnext':
        model = tmodels.resnext101_32x8d(pretrained=True)
    elif arch=='resnet':
        model = tmodels.resnet101(pretrained=True)
    
    log_cb = partial(CSVLogger,filename=log)
    
    print('Creating learner')
    learn = (Learner(data, model, wd=1e-2, opt_func=opt_func,
             metrics=[accuracy,top_k_accuracy],
             bn_wd=False, true_wd=True, 
             path='/home/bartlomiej_cupial',
             callback_fns=[log_cb])
            )
    if freeze:
        print('Freeze activated')
        learn.freeze()
    
    print(learn.path)
    n = len(learn.data.train_dl)
    ann_start2= int(n*epochs*ann_start)
    print(ann_start2," annealing start")


#     learn = learn.to_fp16(dynamic=True),

    if lrfinder:
        print("finding learning rate")
#   run learning rate finder
        IN_NOTEBOOK = 1
        learn.lr_find(wd=1e-2)
        learn.recorder.plot()       
    else:
        print("started training")
        if sched_type == 'one_cycle': 
            learn.fit_one_cycle(epochs, lr, div_factor=10, pct_start=0.3)
        elif sched_type == 'flat_and_anneal': 
            fit_with_annealing(learn, epochs, lr, ann_start)

    if save:
        learn.save(name)

    return learn.recorder.metrics[-1][0]




@call_parse
def main(
        run: Param("Number of run", int)=5,
        lr: Param("Learning rate", float)=1e-3,
        opt: Param("Optimizer (adam,rms,sgd)", str)='adam',
        alpha: Param("Alpha", float)=0.99,
        mom: Param("Momentum", float)=0.9,
        eps: Param("epsilon", float)=1e-6,
        epochs: Param("Number of epochs", int)=5,
        bs: Param("Batch size", int)=64,
        arch: Param("Architecture (xresnet34, xresnet50)", str)='xresnet50',
        sa: Param("Self-attention", int)=0,
        sym: Param("Symmetry for self-attention", int)=0,
        dump: Param("Print model; don't train", int)=0,
        lrfinder: Param("Run learning rate finder; don't train", int)=0,
        sched_type: Param("LR schedule type", str)='one_cycle',
        img_size: Param("Size of image", int)=224,
        path: Param("Path to data", str)='data/cameras_png',
        partial_data: Param("Portion of data used for training", float)=0.2,
        log: Param("Log file name", str)='log',
        ann_start: Param("Mixup", float)=-1.0, 
        save: Param("Save trained model", int)=0,
        name: Param("Name to save", str)='stage-1',
        freeze: Param("freeze pretrained model", int)=0,
        ):

    print(f"optim: {opt}, path: {path}, bs: {bs}, arch: {arch}, img_size: {img_size}")

    acc = np.array(
        [train(lr,opt,alpha,mom,eps,epochs,bs,arch,sa,sym,dump,lrfinder, \
                sched_type,img_size, path, partial_data, log, ann_start, save, freeze)
                for i in range(run)])
    
    print(acc)
    print(np.mean(acc))
    print(np.std(acc))
