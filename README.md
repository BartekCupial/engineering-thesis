# Cameras Classification

## Table of contents
- [Cameras Classification](#cameras-classification)
  - [Table of contents](#table-of-contents)
  - [About](#about)
  - [Training](#training)
  - [Tests](#tests)
  - [Results](#results)
    - [Smaller Dataset](#smaller-dataset)
    - [Full Dataset `TODO`](#full-dataset-todo)
  - [Starting a server](#starting-a-server)
  - [Deployment `TODO`](#deployment-todo)
## About

Model is trained with [fastai](https://www.fast.ai/) and [pytorch](https://pytorch.org/) with images downloaded from  [Dresden Image Database](http://forensics.inf.tu-dresden.de/ddimgdb/).

## Training

To train model first download the data prefferably with `Download Data.ipynb`. To use that notebook you'll need to provide list of urls (which you can download from Dresden Image Database)

All my tests are located in `training/` directiory. My main testing framework `train.py` is prepared for wide range of tests for different hyperparameters.

You can run it in notebook 
```
%run train.py --run 1 --lr 3e-4 --bs 16
```
More parameters can be found inside the `train.py`

## Tests 

* How rectangular images behave in CNN (compared to square ones)
* Which transformations we can perform and still make network converge 
    * flipping
    * warp
    * rotate
    * zoom 
    * crop 
    * contrast
* How different floating point precision affects results
* Test new Mish activation function
* Test [Label Smoothin Cross Entropy](https://arxiv.org/abs/1906.02629)
* Test [BatchLossFilter](https://arxiv.org/pdf/1910.00762.pdf) technique which can speed up the trainig by focusing on bigger losses
* Compare Flatten Anneal with Fit One Cycle as learning policy


## Results



### Smaller Dataset

For training I selected 11 classes from the data set, and 200 pictures from each class.
Notebook can be found [here](https://colab.research.google.com/drive/10mFsCaiOioaifH_VCwWEjlPkKEG9Urxd).
Best accuracy was around 90%, but for two classes - Agfa and Canon it was closer to flip of a coin. I found out, that it was probably result of common parts used in cameras. 

![results of first training](assets/small_ds_matrix.png)

### Full Dataset `TODO`

## Starting a server

You can test with docker 
```
docker build -t cameras . && docker run --rm -it -p 5000:5000 cameras
```
or run locally
```
python app/server.py serve
```

## Deployment `TODO`
Repo prepared to be delpoyed. Created according to example on [fastai](https://course.fast.ai/deployment_render.html#fork-the-starter-app-on-github).